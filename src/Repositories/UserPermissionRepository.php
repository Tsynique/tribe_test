<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Repositories;

use Martynas\TribeTest\Helpers\Db;

class UserPermissionRepository {
    const TABLE_NAME = 'userPermissions';
    const GROUP_PERMISSIONS_PIVOT_TABLE_NAME = 'userGroup2userPermission';

    public function getAllPermissions(): array {
        $columns = [
            'id',
            'title',
        ];
        return Db::fetchRows(self::TABLE_NAME, [], $columns);
    }
}
