<?php

declare(strict_types=1);

namespace Martynas\TribeTest;

use Martynas\TribeTest\Abstracts\ControllerAbstract;
use Throwable;

class Router {
    const DEFAULT_CONTROLLER = 'Users';
    const DEFAULT_ACTION = 'index';

    /**
     *
     * @return void
     */
    public function runRoute(): void {
        if (!isset($_GET['route']) || empty($_GET['route'])) {
            $route = self::DEFAULT_CONTROLLER;
        }

        $route = trim($_GET['route']);
        $path = explode('/', $route);
        if (empty($path[0])) {
            self::notFound();
        }

        $controllerTitle = trim($path[0]);
        $controller = $this->findController($controllerTitle);
        if ($controller === null) {
            self::notFound();
        }

        if (!isset($path[1])) {
            $this->runControllerAction($controller, self::DEFAULT_ACTION);
        } else {
            $action = trim($path[1]);
            $params = array_slice($path, 2);
            $this->runControllerAction($controller, $action, $params);
        }
    }

    /**
     *
     * @param string $controllerTitle
     * @return ControllerAbstract|null
     */
    public function findController(string $controllerTitle): ?ControllerAbstract {
        $className = 'Martynas\\TribeTest\\Controllers\\'.$controllerTitle;

        if (!class_exists($className)) {
            return null;
        }

        return new $className();
    }

    /**
     *
     * @param ControllerAbstract $controller
     * @param string $action
     * @return void
     */
    public function runControllerAction(ControllerAbstract $controller, string $action, array $params = []): void {
        if (!method_exists($controller, $action)) {
            self::notFound();
        }

        call_user_func_array([$controller, $action], $params);
    }

    /**
     *
     * @return void
     */
    public static function notFound(): void {
        http_response_code(404);
        die('404');
    }
}
