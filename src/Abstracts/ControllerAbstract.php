<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Abstracts;

abstract class ControllerAbstract {
    public abstract function index();
}
