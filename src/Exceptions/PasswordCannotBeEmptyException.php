<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Exceptions;

use Exception;

class PasswordCannotBeEmptyException extends Exception {
    
}
