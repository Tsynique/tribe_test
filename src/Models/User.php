<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Models;

use Martynas\TribeTest\Exceptions\PasswordCannotBeEmptyException;
use Martynas\TribeTest\Exceptions\UserModelNotLoaded;
use Martynas\TribeTest\Helpers\Auth;
use Martynas\TribeTest\Helpers\Db;
use Martynas\TribeTest\Helpers\SqlFormatter;
use Martynas\TribeTest\Repositories\UserRepository;

class User {
    const GROUP_PIVOT_TABLE_NAME = 'user2userGroup';
    const PERMISSIONS_TABLE_NAME = 'userPermissions';
    const GROUP_PERMISSIONS_PIVOT_TABLE_NAME = 'userGroup2userPermission';

    /**
     *
     * @var int|null
     */
    private ?int $id = null;

    /**
     *
     * @var string
     */
    private string $username;

    /**
     *
     * @var string
     */
    private string $password;

    /**
     *
     * @var array|null
     */
    private ?array $groupIds = null;

    /**
     *
     * @var array|null
     */
    private ?array $permissions = null;

    /**
     *
     * @param int|null $id
     */
    public function __construct(?int $id = null) {
        if (!empty($id)) {
            $this->loadUserById($id);
        }
    }

    /**
     *
     * @param int $id
     * @return void
     */
    private function loadUserById(int $id): void {
        $where = [
            'id' => $id,
        ];
        $userRow = Db::fetchRow(UserRepository::TABLE_NAME, $where);

        if ($userRow !== null) {
            $this->id = (int)$userRow['id'];
            $this->username = $userRow['username'];
        }
    }

    /**
     *
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getUsername(): string {
        return $this->username;
    }

    /**
     *
     * @param string $username
     * @return void
     */
    public function setUsername(string $username): void {
        $this->username = $username;
    }

    /**
     *
     * @param string $passwordPlain
     * @return void
     */
    public function setPassword(string $passwordPlain): void {
        $this->password = Auth::hashPassword($passwordPlain);
    }

    /**
     *
     * @return bool
     */
    public function save(): bool {
        if ($this->isNew()) {
            return $this->saveAsNew();
        }

        return $this->update();
    }

    /**
     *
     * @return bool
     * @throws PasswordCannotBeEmptyException
     */
    public function saveAsNew(): bool {
        if (!isset($this->password)) {
            throw new PasswordCannotBeEmptyException();
        }

        $row = [
            'username' => $this->username,
            'password' => $this->password,
        ];

        $insertedId = Db::insertRow(UserRepository::TABLE_NAME, $row);

        if ($insertedId !== null) {
            $this->id = $insertedId;
            unset($this->password);
            return true;
        }

        return false;
    }

    /**
     *
     * @return bool
     */
    private function update(): bool {
        $update = [
            'username' => $this->username,
        ];

        if (isset($this->password)) {
            $update['password'] = $this->password;
        }

        $where = [
            'id' => $this->id,
        ];

        return Db::updateRow(UserRepository::TABLE_NAME, $where, $update);
    }

    /**
     *
     * @return bool
     */
    private function isNew(): bool {
        return $this->id === null;
    }

    /**
     *
     * @return array
     * @throws UserModelNotLoaded
     */
    public function getGroupIds(): array {
        if ($this->groupIds !== null) {
            return $this->groupIds;
        }

        if ($this->isNew()) {
            throw new UserModelNotLoaded();
        }

        $where = [
            'user_id' => $this->id,
        ];

        $rows = Db::fetchRows(self::GROUP_PIVOT_TABLE_NAME, $where, ['group_id']);
        $groupIds = array_column($rows, 'group_id');
        $this->groupIds = $groupIds;

        return $this->groupIds;
    }

    /**
     *
     * @return array
     */
    public function getGrantedPermissions(): array {
        if ($this->permissions !== null) {
            return $this->permissions;
        }

        $groupIds = $this->getGroupIds();
        if (count($groupIds) === 0) {
            return [];
        }

        $sql = SqlFormatter::buildSelectStatement(self::PERMISSIONS_TABLE_NAME, ['title'], [], []);

        $clauses = ['`gp`.`permission_id` = `id`'];
        $join = SqlFormatter::buildJoinStatement(self::GROUP_PERMISSIONS_PIVOT_TABLE_NAME, 'gp', $clauses);

        $sqlFull = sprintf(
            '%s %s  WHERE `gp`.`group_id` IN (%s) GROUP BY `id`',
            $sql,
            $join,
            str_repeat('?,', count($groupIds) - 1) . '?',
        );

        $bindings = $groupIds;
        $permissions = Db::fetchRowsFromRawQuery($sqlFull, $bindings);
        $this->permissions = array_column($permissions, 'title');
        return $this->permissions;
    }

    /**
     *
     * @param string $permission
     * @return bool
     */
    public function hasPermission(string $permission): bool {
        $grantedPermissions = $this->getGrantedPermissions();
        return (in_array($permission, $grantedPermissions));
    }

    /**
     *
     * @param array $permissions
     * @return bool
     */
    public function hasPermissions(array $permissions): bool {
        $grantedPermissionsFlipped = array_flip($this->getGrantedPermissions());
        foreach ($permissions as $permission) {
            if (!isset($grantedPermissionsFlipped[$permission])) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     * @param array $groupIds
     * @return void
     * @throws UserModelNotLoaded
     */
    public function changeUserGroups(array $groupIds): void {
        if ($this->isNew()) {
            throw new UserModelNotLoaded();
        }

        $where = [
            'user_id' => $this->id,
        ];
        Db::deleteRows(self::GROUP_PIVOT_TABLE_NAME, $where);

        foreach ($groupIds as $groupId) {
            $row = [
                'user_id' => $this->id,
                'group_id' => $groupId,
            ];
            Db::insertRow(self::GROUP_PIVOT_TABLE_NAME, $row);
        }
    }
}
