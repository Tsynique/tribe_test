<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Controllers;

use Martynas\TribeTest\Abstracts\ControllerAbstract;
use Martynas\TribeTest\Models\User;
use Martynas\TribeTest\Models\UserGroup;
use Martynas\TribeTest\Repositories\UserGroupRepository;
use Martynas\TribeTest\Repositories\UserPermissionRepository;

class Groups extends ControllerAbstract {
    /**
     *
     * @return void
     */
    public function index(): void {
        $groupRepo = new UserGroupRepository();
        $groups = $groupRepo->getAllGroups();
        require_once(__DIR__.'/../../views/groups/list.php');
    }

    /**
     *
     * @return void
     */
    public function edit(int $id): void {
        if (isset($_POST['submit'])) {
            $this->editSave($id);
        }

        $group = new UserGroup($id);
        $groupPermissions = $group->getPermissions();
        $permissionRepo = new UserPermissionRepository();
        $permissions = $permissionRepo->getAllPermissions();

        require_once(__DIR__.'/../../views/groups/edit.php');
    }

    /**
     *
     * @param int $id
     * @return void
     */
    private function editSave(int $id): void {
        $group = new UserGroup($id);
        if (!empty($_POST['title'])) {
            $group->setTitle(trim($_POST['username']));
        }

        $group->save();
        $newPermissionIds = [];
        if (isset($_POST['permissions'])) {
            $newPermissionIds = $_POST['permissions'];
        }

        $group->changePermissions($newPermissionIds);
    }
}
