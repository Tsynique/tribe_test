<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Helpers;

class SqlFormatter {
    /**
     *
     * @param string $table
     * @param array $row
     * @param array $bindings
     * @return string
     */
    public static function buildInsertStatement(string $table, array $row, array $bindings): string {
        $columns = [];
        foreach (array_keys($row) as $column) {
            $columns[] = '`'.$column.'`';
        }

        return sprintf(
            'INSERT INTO `%s` (%s) VALUES (%s)',
            $table,
            implode(', ', $columns),
            implode(', ', array_keys($bindings)),
        );
    }

    /**
     *
     * @param string $table
     * @param array $columns
     * @param array $where
     * @param array $bindings
     * @return string
     */
    public static function buildSelectStatement(string $table, array $columns, array $where, array $bindings): string {
        $columnsToSelect = [];
        foreach ($columns as $column) {
            if ($column === '*' || strpos($column, ' ')) {
                $columnsToSelect[] = $column;
            } else {
                $columnsToSelect[] = '`'.$column.'`';
            }
        }

        $clauses = self::buildClauses($where, $bindings);

        $whereKeyword = '';
        if (count($clauses) > 0) {
            $whereKeyword = ' WHERE ';
        }

        return sprintf(
            'SELECT %s FROM `%s` %s %s',
            implode(', ', $columnsToSelect),
            $table,
            $whereKeyword,
            implode(' AND ', $clauses),
        );
    }

    /**
     *
     * @param string $table
     * @param array $where
     * @param array $bindings
     * @return string
     */
    public static function buildDeleteStatement(string $table, array $where, array $bindings): string {
        $clauses = self::buildClauses($where, $bindings);

        $whereKeyword = '';
        if (count($clauses) > 0) {
            $whereKeyword = ' WHERE ';
        }

        return sprintf(
            'DELETE FROM `%s` %s %s',
            $table,
            $whereKeyword,
            implode(' AND ', $clauses),
        );
    }

    /**
     *
     * @param string $table
     * @param string $alias
     * @param array $clauses
     * @return string
     */
    public static function buildJoinStatement(string $table, string $alias, array $clauses): string {
        return sprintf(
            'JOIN `%s` `%s` ON %s',
            $table,
            $alias,
            implode('AND ', $clauses)
        );
    }

    /**
     *
     * @param string $table
     * @param array $where
     * @param array $update
     * @param array $bindings
     * @return string
     */
    public static function buildUpdateStatement(string $table, array $where, array $update, array $bindings): string {
        $setValues = self::buildSetValues($update, $bindings);
        $clauses = self::buildClauses($where, $bindings, count($update));
        return sprintf(
            'UPDATE `%s` SET %s WHERE %s',
            $table,
            implode(', ', $setValues),
            implode('AND ', $clauses),
        );
    }

    /**
     *
     * @param array $where
     * @param array $bindings
     * @param int $bindingsOffset
     * @return array
     */
    private static function buildClauses(array $where, array $bindings, int $bindingsOffset = 0): array {
        $clauses = [];
        $bindingColumns = array_values(array_flip($bindings));
        $i = $bindingsOffset;
        foreach (array_keys($where) as $key) {
            $binding = $bindingColumns[$i];
            $clauses[] = sprintf('`%s` = %s', $key, $binding);
            ++$i;
        }

        return $clauses;
    }

    /**
     *
     * @param array $where
     * @param array $bindings
     * @param int $bindingsOffset
     * @return array
     */
    private static function buildSetValues(array $where, array $bindings, int $bindingsOffset = 0): array {
        return self::buildClauses($where, $bindings, $bindingsOffset);
    }
}
