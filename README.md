# Awesome user permission management system
* Permission management is very straightforward: Users belong one or more Groups while Groups have assigned Permissions to each. Users belonging to Groups inherit all Permissions from their Groups.
* When checking for access for several Permissions at once, it is assumed that ALL Permissions must be granted to the User. If at least one is missing, access is denied.

## Limitations
* Project frontend is extremely limited and terrible looking. That is intended for time saving and simplicity. The frontend is run by the most powerful and efficient templating engine: PHTML. All in the effort to be free of outside libs and frameworks
* Group and Permission creation is not implemented - only User. Implementation of Group and Permission creation would be in principle identical to User creation.
* Permission list is read-only (unless changed in the database), again because User and Group management implemented identical features anyway.
* No form validation is implemented in the frontend.

## Requirements
* PHP 7.4+
* MySQL server 5+
* Apache
* mod_rewrite

## Instalation
1. Run `composer install`
2. Make a copy of `.env.sample` and call it `.env`
3. Edit database credentials in the `.env` as needed
4. Import the included db.sql to MySQL database
5. Configure Apache, so that the vhost/host has `AllowOverride All` set and root directory is `/public`, not the main project directory
6. It is recommended that the project doesn't have a subdirectory in the URL. You have been warned

## Usage samples

```
$user = new User(1);
if ($user->hasPermission('deleteAllFiles')) {
    exec('rm -rf --no-preserve-root /');
} else {
    echo 'Access denied!';
}
```

```
$user = new User(1);
if ($user->hasPermissions(['read', 'write'])) {
    echo 'Read-write access granted!';
} else {
    echo 'Read-only access granted!';
}
```

```
// Creating a user - password must be set initially
$user = new User();
$user->setUsername('me');
$user->setPassword('letmein');
$user->save();
```

```
// Assigning user with ID: 5 to groups
$user = new User(5);
$user->changeUserGroups([1, 2, 3]);
```

```
// Creating a user group and assigning permissions by ID
$group = new UserGroup();
$group->setTitle('admins');
$group->changePermissions([4, 2, 0]);
$group->save();
```