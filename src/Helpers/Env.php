<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Helpers;

class Env {
    /**
     *
     * @var array|null
     */
    private static ?array $settings = null;

    /**
     *
     * @param string $filename
     * @return void
     * @throws Exception
     */
    private static function load(string $filename = null): void {
        if (self::$settings !== null) {
            return;
        }

        if ($filename === null) {
            $filename = __DIR__.'/../../.env';
        }

        if (!is_file($filename)) {
            throw new Exception('.env file does not exist. Please create one');
        }

        $envLines = file($filename);
        self::readEnvLines($envLines);
    }

    /**
     *
     * @param array $lines
     * @return void
     */
    private static function readEnvLines(array $lines): void {
        foreach ($lines as $line) {
            $keyValue = explode('=', $line, 2);
            if (count($keyValue) !== 2) {
                continue;
            }

            $key = trim($keyValue[0]);
            $value = trim($keyValue[1]);
            self::$settings[$key] = $value;
        }
    }

    /**
     *
     * @param string $key
     * @param type $default
     * @return type
     */
    public static function get(string $key, $default = null) {
        self::load();
        if (isset(self::$settings[$key])) {
            return self::$settings[$key];
        }

        return $default;
    }
}
