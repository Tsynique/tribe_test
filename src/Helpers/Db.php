<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Helpers;

use Exception;
use PDO;
use PDOException;

class Db {
    /**
     *
     * @var PDO
     */
    private static ?PDO $pdo = null;

    /**
     *
     * @throws PDOException
     * @return PDO
     */
    public static function connect(): PDO {
        if (self::$pdo !== null) {
            return self::$pdo;
        }

        $host = Env::get('MYSQL_HOST', 'localhost');
        $db = Env::get('MYSQL_DATABASE');
        $user = Env::get('MYSQL_USERNAME');
        $pass = Env::get('MYSQL_PASSWORD', '');
        $port = Env::get('MYSQL_PORT', 3306);

        $dsn = sprintf('mysql:host=%s;dbname=%s;port=%d', $host, $db, $port);

        self::$pdo = new PDO($dsn, $user, $pass);

        return self::$pdo;
    }

    /**
     *
     * @param array $values
     * @return array
     */
    private static function prepareBindings(array $values): array {
        $bindings = [];
        $i = 1;
        foreach ($values as $value) {
            $bindings[':'.$i] = $value;
            ++$i;
        }

        return $bindings;
    }

    /**
     *
     * @param string $table
     * @param array $row
     * @return int
     * @throws Exception
     */
    public static function insertRow(string $table, array $row): int {
        $pdo = self::connect();

        $bindings = self::prepareBindings($row);
        $sql = SqlFormatter::buildInsertStatement($table, $row, $bindings);
        $prepared = $pdo->prepare($sql);
        if (!$prepared->execute($bindings)) {
            throw new Exception(implode(' ', $prepared->errorInfo()));
        }

        return (int)$pdo->lastInsertId();
    }

    /**
     *
     * @param string $table
     * @param array $where
     * @param array $update
     * @return bool
     * @throws Exception
     */
    public static function updateRow(string $table, array $where, array $update): bool {
        $pdo = self::connect();

        $bindings = self::prepareBindings(array_merge($update, $where));

        $sql = SqlFormatter::buildUpdateStatement($table, $where, $update, $bindings);
        $prepared = $pdo->prepare($sql);
        if (!$prepared->execute($bindings)) {
            throw new Exception(implode(' ', $prepared->errorInfo()));
        }

        return true;
    }

    /**
     *
     * @param string $table
     * @param array $where
     * @param array $columns
     * @return array|null
     * @throws Exception
     */
    public static function fetchRow(string $table, array $where, array $columns = ['*']): ?array {
        $pdo = self::connect();
        $bindings = self::prepareBindings($where);
        $sql = SqlFormatter::buildSelectStatement($table, $columns, $where, $bindings);
        $prepared = $pdo->prepare($sql);

        if (!$prepared->execute($bindings)) {
            throw new Exception(implode(' ', $prepared->errorInfo()));
        }

        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        if ($row !== false) {
            return $row;
        }

        return null;
    }

    /**
     *
     * @param string $table
     * @param array $where
     * @return bool
     * @throws Exception
     */
    public static function deleteRows(string $table, array $where): bool {
        $pdo = self::connect();
        $bindings = self::prepareBindings($where);
        $sql = SqlFormatter::buildDeleteStatement($table, $where, $bindings);
        $prepared = $pdo->prepare($sql);

        if (!$prepared->execute($bindings)) {
            throw new Exception(implode(' ', $prepared->errorInfo()));
        }

        return true;
    }

    /**
     *
     * @param string $table
     * @param array $where
     * @param array $columns
     * @return array|null
     * @throws Exception
     */
    public static function fetchRows(string $table, array $where, array $columns = ['*']): ?array {
        $pdo = self::connect();
        $bindings = self::prepareBindings($where);
        $sql = SqlFormatter::buildSelectStatement($table, $columns, $where, $bindings);

        $prepared = $pdo->prepare($sql);

        if (!$prepared->execute($bindings)) {
            throw new Exception(implode(' ', $prepared->errorInfo()));
        }

        return $prepared->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     *
     * @param string $sql
     * @param array $bindings
     * @return array|null
     * @throws Exception
     */
    public static function fetchRowsFromRawQuery(string $sql, array $bindings = []): ?array {
        $pdo = self::connect();

        $prepared = $pdo->prepare($sql);

        if (!$prepared->execute($bindings)) {
            throw new Exception(implode(' ', $prepared->errorInfo()));
        }

        return $prepared->fetchAll(PDO::FETCH_ASSOC);
    }
}
