<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Helpers;

class Format {
	public static function htmlentities($str) {
		return htmlentities($str, ENT_QUOTES, 'UTF-8', false);
	}
}
