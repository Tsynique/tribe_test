<?php

use Martynas\TribeTest\Helpers\Format;

include(__DIR__.'/../header.html');

?>
<h1>EDIT GROUP</h1>
<form action="" method="post" autocomplete="off">
    <table>
        <tr>
            <td>Title: </td>
            <td><input type="text" name="username" value="<?= Format::htmlentities($group->getTitle()) ?>" /></td>
        </tr>
        <tr>
            <td>Permissions: </td>
            <td>
                <select multiple name="permissions[]">
                    <?php
                        foreach ($permissions as $permission) {
                            $selected = '';
                            if (in_array($permission['title'], $groupPermissions)) {
                                $selected = 'selected';
                            }
                            printf(
                                '<option value="%d" %s>%s</option>',
                                $permission['id'],
                                $selected,
                                Format::htmlentities($permission['title'])
                            );
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Save" /></td>
        </tr>
    </table>
</form>

<?php
include(__DIR__.'/../footer.html');