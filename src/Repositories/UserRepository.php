<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Repositories;

use Martynas\TribeTest\Helpers\Db;
use Martynas\TribeTest\Models\User;

class UserRepository {
    const TABLE_NAME = 'users';

    public function getAllUsers(): array {
        $columns = [
            'id',
            'username',
        ];
        return Db::fetchRows(self::TABLE_NAME, [], $columns);
    }
}
