<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Controllers;

use Martynas\TribeTest\Abstracts\ControllerAbstract;

class Permissions extends ControllerAbstract {
    public function index(): void {
        die('permissions');
    }

    public function insert(): void {

    }

    public function delete(): void {

    }
}
