<?php

include(__DIR__.'/../header.html');

?>

<h1>GROUPS</h1>
<table style="width: 100%" border="1">
    <tr>
        <th>Group</th>
        <th>Actions</th>
    </tr>
<?php

foreach ($groups as $group) {
    echo '<tr>';

    echo '<td>'.$group['title'].'</td>';
    echo '<td><a href="/Groups/edit/'.$group['id'].'">Edit</a></td>';
    echo '</tr>';
}

?>
</table>
<?php

include(__DIR__.'/../footer.html');