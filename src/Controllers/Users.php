<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Controllers;

use Martynas\TribeTest\Abstracts\ControllerAbstract;
use Martynas\TribeTest\Models\User;
use Martynas\TribeTest\Repositories\UserGroupRepository;
use Martynas\TribeTest\Repositories\UserRepository;

class Users extends ControllerAbstract {
    /**
     *
     * @return void
     */
    public function index(): void {
        $userRepo = new UserRepository();
        $users = $userRepo->getAllUsers();
        require_once(__DIR__.'/../../views/users/list.php');
    }

    /**
     *
     * @return void
     */
    public function edit(int $id): void {
        if (isset($_POST['submit'])) {
            $this->editSave($id);
        }

        $user = new User($id);
        $userGroupIds = $user->getGroupIds();
        $groupRepo = new UserGroupRepository();
        $groups = $groupRepo->getAllGroups();

        require_once(__DIR__.'/../../views/users/edit.php');
    }

    /**
     *
     * @param int $id
     * @return void
     */
    private function editSave(int $id): void {
        $user = new User($id);
        if (!empty($_POST['username'])) {
            $user->setUsername(trim($_POST['username']));
        }

        if (!empty($_POST['password'])) {
            $user->setPassword(trim($_POST['password']));
        }

        $user->save();
        $newGroupIds = [];
        if (isset($_POST['groups'])) {
            $newGroupIds = $_POST['groups'];
        }

        $user->changeUserGroups($newGroupIds);
    }

    /**
     *
     * @return void
     */
    public function create(): void {
        if (isset($_POST['submit'])) {
            $newId = $this->createSave();
            if ($newId !== null) {
                header('Location: /Users/edit/'.$newId);
                die;
            }
        }

        $groupRepo = new UserGroupRepository();
        $groups = $groupRepo->getAllGroups();
        require_once(__DIR__.'/../../views/users/create.php');
    }

    /**
     *
     * @return void
     */
    private function createSave(): ?int {
        $user = new User();
        if (empty($_POST['username']) || empty($_POST['password'])) {
            return null;
        }

        $user->setUsername(trim($_POST['username']));
        $user->setPassword(trim($_POST['password']));

        $user->save();

        $newGroupIds = [];
        if (isset($_POST['groups'])) {
            $newGroupIds = $_POST['groups'];
        }

        $user->changeUserGroups($newGroupIds);

        return $user->getId();
    }
}
