<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Models;

use Martynas\TribeTest\Helpers\Db;
use Martynas\TribeTest\Helpers\SqlFormatter;
use Martynas\TribeTest\Repositories\UserGroupRepository;
use Martynas\TribeTest\Repositories\UserPermissionRepository;

class UserGroup {
    /**
     *
     * @var int|null
     */
    private ?int $id = null;

    /**
     *
     * @var string
     */
    private string $title;

    /**
     *
     * @var array|null
     */
    private ?array $permissions = null;

    /**
     *
     * @param int|null $id
     */
    public function __construct(?int $id = null) {
        if (!empty($id)) {
            $this->loadGroupById($id);
        }
    }

    /**
     *
     * @param int $id
     * @return void
     */
    private function loadGroupById(int $id): void {
        $where = [
            'id' => $id,
        ];
        $groupRow = Db::fetchRow(UserGroupRepository::TABLE_NAME, $where);

        if ($groupRow !== null) {
            $this->id = (int)$groupRow['id'];
            $this->title = $groupRow['title'];
        }
    }

    /**
     *
     * @return bool
     */
    public function save(): bool {
        if ($this->isNew()) {
            return $this->saveAsNew();
        }

        return $this->update();
    }

    /**
     *
     * @return bool
     */
    public function saveAsNew(): bool {
        $row = [
            'title' => $this->title,
        ];

        $insertedId = Db::insertRow(UserGroupRepository::TABLE_NAME, $row);

        if ($insertedId !== null) {
            $this->id = $insertedId;
            return true;
        }

        return false;
    }

    /**
     *
     * @return bool
     */
    private function update(): bool {
        $update = [
            'title' => $this->title,
        ];

        $where = [
            'id' => $this->id,
        ];

        return Db::updateRow(UserGroupRepository::TABLE_NAME, $where, $update);
    }

    /**
     *
     * @return bool
     */
    private function isNew(): bool {
        return $this->id === null;
    }

    /**
     *
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     *
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     *
     * @return array
     */
    public function getPermissions(): array {
        if ($this->permissions !== null) {
            return $this->permissions;
        }

        $sql = SqlFormatter::buildSelectStatement(UserPermissionRepository::TABLE_NAME, ['title'], [], []);

        $clauses = ['`gp`.`permission_id` = `id`'];
        $join = SqlFormatter::buildJoinStatement(
            UserPermissionRepository::GROUP_PERMISSIONS_PIVOT_TABLE_NAME,
            'gp',
            $clauses
        );

        $sqlFull = sprintf(
            '%s %s  WHERE `gp`.`group_id` = %d GROUP BY `id`',
            $sql,
            $join,
            $this->id,
        );

        $permissions = Db::fetchRowsFromRawQuery($sqlFull);
        $this->permissions = array_column($permissions, 'title');
        return $this->permissions;
    }

    /**
     *
     * @param array $permissionIds
     * @return void
     */
    public function changePermissions(array $permissionIds): void {
        $where = [
            'group_id' => $this->id,
        ];
        Db::deleteRows(UserPermissionRepository::GROUP_PERMISSIONS_PIVOT_TABLE_NAME, $where);

        foreach ($permissionIds as $permissionId) {
            $row = [
                'group_id' => $this->id,
                'permission_id' => $permissionId,
            ];
            Db::insertRow(UserPermissionRepository::GROUP_PERMISSIONS_PIVOT_TABLE_NAME, $row);
        }
    }
}
