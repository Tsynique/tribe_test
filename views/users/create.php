<?php

use Martynas\TribeTest\Helpers\Format;

include(__DIR__.'/../header.html');

?>
<h1>CREATE USER</h1>
<form action="" method="post" autocomplete="off">
    <table>
        <tr>
            <td>Username: </td>
            <td><input type="text" name="username" value="" /></td>
        </tr>
        <tr>
            <td>Set password: </td>
            <td><input type="text" name="password" value="" /></td>
        </tr>
        <tr>
            <td>Groups: </td>
            <td>
                <select multiple name="groups[]">
                    <?php
                        foreach ($groups as $group) {
                            $selected = '';
                            printf(
                                '<option value="%d" %s>%s</option>',
                                $group['id'],
                                $selected,
                                Format::htmlentities($group['title'])
                            );
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Save" /></td>
        </tr>
    </table>
</form>

<?php
include(__DIR__.'/../footer.html');