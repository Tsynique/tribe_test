<?php

include(__DIR__.'/../header.html');

?>

<h1>USERS</h1>
<a href="/Users/create">Add new</a>
<table style="width: 100%" border="1">
    <tr>
        <th>Username</th>
        <th>Actions</th>
    </tr>
<?php

foreach ($users as $user) {
    echo '<tr>';

    echo '<td>'.$user['username'].'</td>';
    echo '<td><a href="/Users/edit/'.$user['id'].'">Edit</a></td>';
    echo '</tr>';
}

?>
</table>
<?php

include(__DIR__.'/../footer.html');