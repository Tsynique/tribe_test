<?php

declare(strict_types=1);

namespace Martynas\TribeTest\Repositories;

use Martynas\TribeTest\Helpers\Db;

class UserGroupRepository {
    const TABLE_NAME = 'userGroups';

    public function getAllGroups(): array {
        $columns = [
            'id',
            'title',
        ];
        return Db::fetchRows(self::TABLE_NAME, [], $columns);
    }
}
